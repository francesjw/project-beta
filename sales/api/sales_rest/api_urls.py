from django.urls import path
from .views import api_list_salespeople, api_list_customer, api_salesperson, api_customer, api_list_sales, api_sale

urlpatterns = [
    path("salespeople/", api_list_salespeople, name="api_list_salespeople"),
    path("salespeople/<int:id>/", api_salesperson, name="api_salesperson"),
    path("customers/", api_list_customer, name="api_list_customer"),
    path("customers/<int:id>/", api_customer, name="api_customer"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:id>/", api_sale, name="api_sales"),
]
