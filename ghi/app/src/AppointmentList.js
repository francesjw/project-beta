import React, { useState, useEffect } from 'react';

function AppointmentList() {
    const [list, setList] = useState(null);
    
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setList(data.appointments);
        }
    }
    const deleteAppointment = async (id) => {
        const deleteUrl = `http://localhost:8080/api/appointments/${id}/`;
        const response = await fetch(deleteUrl, {method: 'DELETE'});
        fetchData()
    }
    const finishAppointment = async (id) => {
        const finishedUrl = `http://localhost:8080/api/appointments/${id}/`;
        const status = {"status": "COMPLETE"}
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(status),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(finishedUrl, fetchConfig);
        fetchData()
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <>
        <h1>Service Appointments</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <td>Vin</td>
                    <td>Is Vip?</td>
                    <td>Customer</td>
                    <td>Date</td>
                    <td>Time</td>
                    <td>Technician</td>
                    <td>Reason</td>
                </tr>
            </thead>
            <tbody>
            {list?.filter((appointments) => appointments.status !== "COMPLETE")
                    ?.map(appointment => {
                    return (
                    <tr key={appointment.id}>
                        <td>{appointment.vin}</td>
                        <td>{appointment.vip}</td>
                        <td>{appointment.customer}</td>
                        <td>{appointment.date}</td>
                        <td>{appointment.time}</td>
                        <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                        <td>{appointment.reason}</td>
                        <td><button onClick={() => deleteAppointment(appointment.id)} className="btn btn-danger">Cancel</button></td>
                        <td><button onClick={() => finishAppointment(appointment.id)} name="status" value={appointment.status} className="btn btn-success">Finished</button></td>
                    </tr>
                )
            })}
            </tbody>
        </table>
        </>
    )
}

export default AppointmentList;
