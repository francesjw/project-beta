import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList'
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import VehicleModelForm from './VehicleModelForm';
import VehicleModelList from './VehicleModelList';
import SalespeopleList from './SalespeopleList';
import SalespersonForm from './SalespersonForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import TechnicianForm from './TechnicianForm'
import TechnicianList from './TechnicianList';
import SalesList from './SalesList';
import SaleForm from './SaleForm';
import SalespersonHistory from './SalespersonHistory';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AppointmentHistory from './AppointmentHistory';

function App(props) {
  if (props.manufacturers === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="/manufacturers" element={<ManufacturerList manufacturers={props.manufacturers}/>} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="/automobiles" element={<AutomobileList automobiles={props.automobiles}/>} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="vehicleModels">
            <Route path="/vehicleModels" element={<VehicleModelList vehicleModels={props.vehicleModels}/>} />
            <Route path="new" element={<VehicleModelForm />} />
          </Route>
          <Route path="salespeople">
            <Route path="/salespeople" element={<SalespeopleList salesPeople={props.salesPeople}/>} />
            <Route path="new" element={<SalespersonForm />} />
          </Route>
          <Route path="customers">
            <Route path="/customers" element={<CustomerList customers={props.customers} />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="technicians">
            <Route path="/technicians" element={<TechnicianList technicians={props.technicians} />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="sales">
            <Route path="/sales" element={<SalesList sales={props.sales} />} />
            <Route path="new" element={<SaleForm />} />
          </Route>
          <Route path="salespersonhistory">
            <Route path="/salespersonhistory" element={<SalespersonHistory />} />
          </Route>
          <Route path="appointments">
            <Route path="/appointments" element={<AppointmentList appointments={props.appointments} />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path= "history" element={<AppointmentHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
