import React, {useEffect, useState} from 'react';

function SaleForm() {
    const [automobiles, setAutos] = useState([]);
    const [salesPeople, setSalesPeople] = useState([]);
    const [customers, setCustomers] = useState([]);

    const [automobile, setAutomobile] = useState('');
    const [salesPersonId, setSalesPersonId] = useState('');
    const [customerId, setCustomerId] = useState('');
    const [price, setPrice] = useState('');

    const handleAutoChange = event => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleSalesPersonChange = event => {
        const value = event.target.value;
        setSalesPersonId(value);
    }

    const handleCustomerChange = event => {
        const value = event.target.value;
        setCustomerId(value);
    }

    const handlePriceChange = event => {
        const value = event.target.value;
        setPrice(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.price = price;
        data.automobile = automobile;
        data.salesperson_id = salesPersonId;
        data.customer_id = customerId;

        const saleUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                }
            }
        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            const autoData = {...automobile, sold: true};
            const putConfig = {
                method: "put",
                body: JSON.stringify(autoData),
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            const autoResponse = await fetch(
                `http://localhost:8100/api/automobiles/${automobile}/`,
                putConfig,
                );
            if (autoResponse.ok) {
                setPrice('');
                setAutomobile('');
                setSalesPersonId('');
                setCustomerId('');
        }
        }
    }

    const fetchData = async () => {
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const salesPeopleUrl = 'http://localhost:8090/api/salespeople/';
        const customerUrl = 'http://localhost:8090/api/customers/';

        const autoResponse = await fetch(autoUrl);
        const salesPeopleResponse = await fetch(salesPeopleUrl);
        const customerResponse = await fetch(customerUrl);

        if (autoResponse.ok || salesPeopleResponse.ok || customerResponse.ok) {
            const autoData = await autoResponse.json();
            const salesPeopleData = await salesPeopleResponse.json();
            const customerData = await customerResponse.json();
            const filterAutos = autoData.autos.filter(autos => {
                return(
                    autos.sold == false
                )
            })
            setAutos(filterAutos);
            setSalesPeople(salesPeopleData.salespeople);
            setCustomers(customerData.customers);
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Record a Sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
                <select onChange={handleAutoChange} value={automobile} required name="automobile" id="automobile" className="form-select" >
                <option value="">Choose VIN</option>
                {automobiles.map(automobile => {
                    return (
                    <option key={automobile.vin} value={automobile.vin}>
                        {automobile.vin}
                    </option>
                    );
                })}
                </select>
                </div>
                <div className="mb-3">
                <select onChange={handleSalesPersonChange} value={salesPersonId} required name="salesperson_id" id="salesperson_id" className="form-select" >
                <option value="">Choose a salesperson</option>
                {salesPeople.map(salesPerson => {
                    return (
                    <option key={salesPerson.id} value={salesPerson.id}>
                        {salesPerson.first_name} {salesPerson.last_name}
                    </option>
                    );
                })}
                </select>
                </div>
                <div className="mb-3">
                <select onChange={handleCustomerChange} value={customerId} required name="customer_id" id="customer_id" className="form-select" >
                <option value="">Choose a customer</option>
                {customers.map(customer => {
                    return (
                    <option key={customer.id} value={customer.id}>
                        {customer.first_name} {customer.last_name}
                    </option>
                    );
                })}
                </select>
                </div>
            <div className="form-floating mb-3">
                <input onChange={handlePriceChange} value={price} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
                <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Add</button>
            </form>
        </div>
        </div>
    </div>
    )
}

export default SaleForm;
