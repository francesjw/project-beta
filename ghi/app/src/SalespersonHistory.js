import React, {useState, useEffect} from 'react';


function SalesDetails(props) {
    const {sales, salespersonId} = props;
    const filteredSales = sales.filter(sale => sale.salesperson.id == salespersonId);
    return (
        <>
        <table className="table table-striped">
        <thead>
        <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
            </tr>
        </thead>
        <tbody>
        {filteredSales.map(sale => {
            return(
                <tr key={sale.id}>
                        <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                        <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                        <td>{sale.automobile.vin}</td>
                        <td>{sale.price }</td>
                        </tr>
            )
            })}
        </tbody>
        </table>
        </>
    )
}


function SalesPersonHistory(props) {
    const [salespeople, setSalesPeople] = useState([]);
    const [seletectedSalesperson, setSeletectedSalesperson] = useState(null);
    const [sales, setSales] = useState([]);

    const handleClick = async (salespersonId) => {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            const filterSales = data.sales.filter(sale => {
                return (
                    sale.salesperson.id == salespersonId
                    )
                });
            setSales(filterSales);
            setSeletectedSalesperson(salespersonId);
        }
    }

    const fetchData = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const salesUrl = 'http://localhost:8090/api/sales/';
        const salespeopleResponse = await fetch(salespeopleUrl);
        const salesResponse = await fetch(salesUrl);
        if (salespeopleResponse.ok || salesResponse.ok) {
            const salespeopleData = await salespeopleResponse.json();
            const salesData = await salesResponse.json();
            setSalesPeople(salespeopleData.salespeople);
            setSales(salesData.sales);
        }
    }
useEffect(() => {
    fetchData();
}, []);

    return(
        <div className="row">
            <div className="col-6">
                <h1>Sales History</h1>
                <div className="mb-3">
                    {salespeople.map(salesperson => {})}
                <select onChange={(event) => handleClick(event.target.value)} required name="salesperson" id="salesperson" className="form-select" >
                    <option value="">Choose a salesperson</option>
                {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>
                        {salesperson.first_name} {salesperson.last_name}
                        </option>
                    );
                })}
                </select>
            </div>
            </div>
            <div className="row">
            <div>
                {<SalesDetails sales={sales} salespersonId={seletectedSalesperson} />}
            </div>
        </div>
    </div>
    )
}


export default SalesPersonHistory;
