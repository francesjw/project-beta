import React, {useEffect, useState} from 'react';


function AppointmentForm() {

    const [technicians, setTechnicians] = useState([]);
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [technicianId, setTechnicianId] = useState('');
    const [vin, setVin] = useState('');
    const [reason, setReason] = useState('');
    const [vip, setVip] = useState(false);

    const handleVinChange = event => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerChange = event => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleDateChange = event => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTimeChange = event => {
        const value = event.target.value;
        setTime(value);
    }

    const handleTechnicianChange = event => {
        const value = event.target.value;
        setTechnicianId(value);
    }

    const handleVipChange = (event) => {
        const isChecked = event.target.checked;
        setVip(isChecked);
    };

    const handleReasonChange = event => {
        const value = event.target.value;
        setReason(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date = date;
        data.time = time;
        data.technician_id = technicianId;
        data.reason = reason;
        data.vip = vip;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                }
            }

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            setVin('');
            setCustomer('');
            setDate('');
            setTime('');
            setTechnicianId('');
            setReason('');
            setVip('');
        };
    }

    const fetchData = async () => {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const technicianResponse = await fetch(technicianUrl);
        if (technicianResponse.ok) {
            const technicianData = await technicianResponse.json();
            setTechnicians(technicianData.technicians);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
            <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a service appointment </h1>
                <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                    <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                    <label htmlFor="vin">Vin</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleCustomerChange} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                    <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleDateChange} value={date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                    <label htmlFor="date">Date</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleTimeChange} value={time} placeholder="Time" required type="time" name="time" id="coltimeor" className="form-control" />
                    <label htmlFor="time">Time</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                    <label htmlFor="reason">Reason</label>
                </div>
                <div className="form-check mb-3">
                    <input onChange={handleVipChange} checked={vip} type="checkbox" name="vip" id="vip" className="form-check-input" />
                    <label htmlFor="vip" className="form-check-label">Is Vip</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleTechnicianChange} value={technicianId} required name="technician_id" id="technician_id" className="form-select" >
                    <option value="">Choose a technician</option>
                    {technicians.map(technician => {
                        return (
                        <option key={technician.id} value={technician.id}>
                            {technician.first_name} {technician.last_name}
                        </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Add</button>
                </form>
            </div>
            </div>
        </div>
    )
}

    export default AppointmentForm;
