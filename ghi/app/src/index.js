import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadData() {
  const manufacturerResponse = await fetch('http://localhost:8100/api/manufacturers/');
  const automobileResponse = await fetch('http://localhost:8100/api/automobiles/');
  const vehicleModelResponse = await fetch('http://localhost:8100/api/vehicleModels/');
  const salesPeopleResponse = await fetch('http://localhost:8090/api/salespeople/');
  const customerResponse = await fetch('http://localhost:8090/api/customers/');
  const technicianResponse = await fetch('http://localhost:8080/api/technicians/');
  const salesResponse = await fetch('http://localhost:8090/api/sales/');

  if (manufacturerResponse.ok || automobileResponse.ok ||
    vehicleModelResponse.ok || salesPeopleResponse.ok ||
    customerResponse.ok || technicianResponse.ok || salesResponse.ok
    ) {
    const manufacturerData = await manufacturerResponse.json();
    const autmobileData = await automobileResponse.json();
    const vehicleModelData = await vehicleModelResponse.json();
    const salesPeopleData = await salesPeopleResponse.json();
    const customerData = await customerResponse.json();
    const technicianData = await technicianResponse.json();
    const salesData = await salesResponse.json();
    root.render(
      <React.StrictMode>
        <App manufacturers={manufacturerData.manufacturers}
        automobiles={autmobileData.autos} vehicleModels={vehicleModelData.models}
        salesPeople={salesPeopleData.salespeople} customers={customerData.customers}
        technicians={technicianData.technicians} sales={salesData.sales}
        />
      </React.StrictMode>
    );
  } else {
    console.error(manufacturerResponse);
    console.error(automobileResponse);
    console.error(vehicleModelResponse);
    console.error(salesPeopleResponse);
    console.error(customerResponse);
    console.error(technicianResponse);
    console.error(salesResponse)
  }
}
loadData();
