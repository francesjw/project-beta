import React, {useEffect, useState} from 'react';

function VehicleModelForm() {

    const [name, setName] = useState("");
    const [manufacturers, setManufacturers] = useState([]);
    const [manufacturer, setManufacturerId] = useState('');
    const [picture_url, setPicture_url] = useState("");

    const handleNameChange = event => {
        const value = event.target.value;
        setName(value);
    }


    const handlePicture_urlChange = (event) => {
        setPicture_url(event.target.value);
    }


    const handleManufacturerChange = event => {
        const value = event.target.value;
        setManufacturerId(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.manufacturer_id = manufacturer;
        data.picture_url = picture_url;


        const vehicleModelUrl = 'http://localhost:8100/api/vehicleModels/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                }
            }

        const response = await fetch(vehicleModelUrl, fetchConfig);
        if (response.ok) {
            const newVehicleModel = await response.json();
            console.log(newVehicleModel);
            setName('');
            setPicture_url('')
            setManufacturerId('');
        }
    }


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
    <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a vehicle model</h1>
                <form onSubmit={handleSubmit} id="create-vehicleModel-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Model Name</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handlePicture_urlChange} value={picture_url} placeholder="Picture URL" type="text" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="picture_url">Picture URL</label>
            </div>

                <div className="mb-3">
                    <select onChange={handleManufacturerChange} value={manufacturer} required name="manufacturer" id="manufacturer" className="form-select" >
                    <option value="">Choose a manufacturer</option>
                    {manufacturers.map(manufacturer => {
                        return (
                        <option key={manufacturer.id} value={manufacturer.id}>
                            {manufacturer.name}
                        </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Add</button>
                </form>
            </div>
            </div>
        </div>
        )
    }

    export default VehicleModelForm;
