import React, {useEffect, useState} from 'react';


function AutomobileForm() {
    const [vehicleModels, setVehicleModels] = useState([]);
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model_id, setModelId] = useState('');

    const handleColorChange = event => {
        const value = event.target.value;
        setColor(value);
    }

    const handleYearChange = event => {
        const value = event.target.value;
        setYear(value);
    }

    const handleVinChange = event => {
        const value = event.target.value;
        setVin(value);
    }

    const handleManufacturerChange = event => {
        const value = event.target.value;
        setModelId(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id= model_id;

        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                }
            }

        const response = await fetch(automobileUrl, fetchConfig);
        if (response.ok) {
            const newAutomobile = await response.json();
            setColor('');
            setYear('');
            setVin('');
            setModelId('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/vehicleModels/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setVehicleModels(data.models);
        }
    }

    useEffect(() => {
        fetchData();
        }, []);

    return (
    <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add an automobile to inventory</h1>
                <form onSubmit={handleSubmit} id="create-automobile-form">
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleYearChange} value={year} placeholder="Year" required type="text" name="year" id="year" className="form-control"  />
                    <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                    <label htmlFor="vin">VIN</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleManufacturerChange} value={model_id} required name="model_id" id="model_id" className="form-select" >
                    <option value="">Choose a model</option>
                    {vehicleModels.map(model => {
                        return (
                        <option key={model.id} value={model.id}>
                            {model.name}
                        </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Add</button>
                </form>
            </div>
            </div>
        </div>
        )
    }

    export default AutomobileForm;
