import React, {useEffect, useState} from 'react';

function VehicleModelList() {

    const [vehicleModels, setVehicleModel] = useState([]);

    useEffect(() => {
        const getVehicleModelData = async () => {
        const vehicleModelResponse = await fetch(
            "http://localhost:8100/api/vehicleModels/"
        );
        const vehicleModelData = await vehicleModelResponse.json();
        setVehicleModel(vehicleModelData.models);
        };
    
        getVehicleModelData();
    }, []);
    
    
    return (
        <>
        <h1>Vehicle Models</h1>
        <table className="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
            </tr>
        </thead>
        <tbody>
            {vehicleModels.map((vehicleModel) => {
                return (
                    <tr key={vehicleModel.id}>
                        <td>{vehicleModel.name}</td>
                        <td>{vehicleModel.manufacturer.name}</td>
                        <td>
                            <img src={vehicleModel.picture_url} style={{ width: "150px" }} />
                        </td>
                    </tr>
                )
            })}
        </tbody>
        </table>
        </>
    )
}

export default VehicleModelList;
