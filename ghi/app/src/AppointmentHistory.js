import React, { useState, useEffect } from 'react';

function AppointmentHistory() {
     const [list, setList] = useState(null);

     const fetchData = async () => {
          const url = 'http://localhost:8080/api/appointments/';
          const response = await fetch(url);
          if (response.ok) {
               const data = await response.json();
               setList(data.appointments)
          }
     }

     const [query, setQuery] = useState('')

     useEffect(() => {
          fetchData()
     }, []);
     
return (
     <div>
     <br/>
     <h1>Service History</h1>
     <input type="search" placeholder="Search by VIN" className="form-control" onChange={(e)=>setQuery(e.target.value)} />
     <br/>
     <table className="table table-striped">
          <thead>
               <tr>
                    <th>Vin</th>
                    <th>Customer</th>
                    <th>Vip</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Tecnician</th>
                    <th>Reason</th>
                    <th>Status</th>
               </tr>
          </thead>
          <tbody>
               {list?.filter((appointments) =>
               appointments.vin.includes(query)
               ).map(appointment => {
                    return (
                    <tr key={appointment.id}>
                         <td>{appointment.vin}</td>
                         <td>{appointment.customer}</td>
                         <td>{appointment.vip}</td>
                         <td>{appointment.date}</td>
                         <td>{appointment.time}</td>
                         <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                         <td>{appointment.reason}</td>
                         <td>{appointment.status}</td>
                         </tr>
                    )
               })}
          </tbody>
     </table>
</div>
)
}

export default AppointmentHistory
