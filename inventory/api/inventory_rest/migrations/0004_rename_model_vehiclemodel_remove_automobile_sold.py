# Generated by Django 4.0.3 on 2023-04-25 17:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory_rest', '0003_rename_vehiclemodel_model'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Model',
            new_name='VehicleModel',
        ),
        migrations.RemoveField(
            model_name='automobile',
            name='sold',
        ),
    ]
